﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContextualAutoClicker.Converters
{
    class PointToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo language)
        {
            if(value.GetType() != typeof(Point))
            {
                return "X: ~\nY: ~";
            }

            Point p = (Point)value;

            return "X: " + p.X + "\nY: " + p.Y;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo language)
        {
            throw new NotImplementedException();
        }
    }
}
