﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContextualAutoClicker.Models
{
    public class LogMessage : INotifyPropertyChanged
    {
        public LogMessage()
        {

        }

        public LogMessage(string message, ELogLevel level = ELogLevel.INFO) : this()
        {
            this.Message = message;
            this.Level = level;
        }

        #region PROPERTIES
        private ELogLevel _Level;
        public ELogLevel Level
        {
            get
            {
                return _Level;
            }
            set
            {
                if(_Level != value)
                {
                    _Level = value;
                    OnPropertyChanged("Level");
                }
            }
        }

        private string _Message;
        public string Message
        {
            get
            {
                return _Message;
            }
            set
            {
                if(_Message != value)
                {
                    _Message = value;
                    OnPropertyChanged("Message");
                }
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
