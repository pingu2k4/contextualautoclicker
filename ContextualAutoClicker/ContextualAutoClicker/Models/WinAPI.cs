﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ContextualAutoClicker.Models
{
    class WinAPI
    {
        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out System.Drawing.Point lpPoint);

        public static System.Drawing.Point GetCursorPosition()
        {
            System.Drawing.Point lpPoint;
            GetCursorPos(out lpPoint);

            return lpPoint;
        }

        public static Bitmap GetFullScreenShot()
        {
            var bmpScreenshot = new Bitmap((int)SystemParameters.VirtualScreenWidth, (int)SystemParameters.VirtualScreenHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            // Create a graphics object from the bitmap.
            var gfxScreenshot = Graphics.FromImage(bmpScreenshot);

            gfxScreenshot.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            gfxScreenshot.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            gfxScreenshot.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            // Take the screenshot from the upper left corner to the right bottom corner.
            gfxScreenshot.CopyFromScreen(0, 0, 0, 0, new System.Drawing.Size((int)SystemParameters.VirtualScreenWidth, (int)SystemParameters.VirtualScreenHeight), CopyPixelOperation.SourceCopy);

            return bmpScreenshot;
        }

        public static Bitmap CropImage(Bitmap source, System.Drawing.Point topLeft, System.Drawing.Size size)
        {
            Rectangle cropRect = new Rectangle(topLeft.X, topLeft.Y, size.Width, size.Height);

            Bitmap croppedImage = new Bitmap(cropRect.Width, cropRect.Height);

            using (Graphics g = Graphics.FromImage(croppedImage))
            {
                g.DrawImage(source, new Rectangle(0, 0, croppedImage.Width, croppedImage.Height), cropRect, GraphicsUnit.Pixel);
            }

            return croppedImage;
        }

        public static Bitmap CropImage(Bitmap source, Int32Rect searchArea)
        {
            Rectangle cropRect = new Rectangle(searchArea.X, searchArea.Y, searchArea.Width, searchArea.Height);

            Bitmap croppedImage = new Bitmap(cropRect.Width, cropRect.Height);

            using (Graphics g = Graphics.FromImage(croppedImage))
            {
                g.DrawImage(source, new Rectangle(0, 0, croppedImage.Width, croppedImage.Height), cropRect, GraphicsUnit.Pixel);
            }

            return croppedImage;
        }

        public static Rectangle? Compare(Bitmap sourceImage, Bitmap searchImage, double tolerance)
        {
            Rectangle? returnRect = null;
            Image<Bgr, byte> source = new Image<Bgr, byte>(sourceImage);
            Image<Bgr, byte> template = new Image<Bgr, byte>(searchImage);
            //Image<Bgr, byte> imageToShow = source.Copy();

            using (Image<Gray, float> result = source.MatchTemplate(template, TemplateMatchingType.CcoeffNormed))
            {
                double[] minValues, maxValues;
                System.Drawing.Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);

                if (maxValues[0] > tolerance)
                {
                    // This is a match. Do something with it, for example draw a rectangle around it.
                    //imageToShow.Draw(match, new Bgr(Color.Red), 3);
                    returnRect = new Rectangle(maxLocations[0], template.Size);
                }
            }

            source.Dispose();
            template.Dispose();

            return returnRect;
        }

        public static double GetComparisonScore(Bitmap sourceImage, Bitmap searchImage)
        {
            double returnValue = 0;
            Image<Bgr, byte> source = new Image<Bgr, byte>(sourceImage);
            Image<Bgr, byte> template = new Image<Bgr, byte>(searchImage);
            //Image<Bgr, byte> imageToShow = source.Copy();

            using (Image<Gray, float> result = source.MatchTemplate(template, TemplateMatchingType.CcoeffNormed))
            {
                double[] minValues, maxValues;
                System.Drawing.Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);
                returnValue = maxValues[0];
            }

            source.Dispose();
            template.Dispose();

            return returnValue;
        }

        public static Rectangle? FindImageOnScreen(Bitmap sourceImage, Bitmap searchImage, Int32Rect searchArea, double tolerance)
        {
            if (searchArea.Width < searchImage.Width || searchArea.Height < searchImage.Height)
                throw new Exception("search area was smaller than image dimensions.");

            return Compare(CropImage(sourceImage, searchArea), searchImage, tolerance);
        }

        public static Rectangle? FindImageOnScreen(Bitmap sourceImage, Bitmap searchImage, double tolerance)
        {
            return Compare(sourceImage, searchImage, tolerance);
        }

        public static double GetImageMatchScore(Bitmap sourceImage, Bitmap searchImage, Int32Rect searchArea)
        {
            if (searchArea.Width < searchImage.Width || searchArea.Height < searchImage.Height)
                throw new Exception("search area was smaller than image dimensions.");

            return GetComparisonScore(CropImage(sourceImage, searchArea), searchImage);
        }

        #region MOUSE
        [DllImport("User32.Dll", SetLastError = true)]
        public static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public const int MOUSE_LeftDown = 0x00000002;
        public const int MOUSE_LeftUp = 0x00000004;
        public const int MOUSE_MiddleDown = 0x00000020;
        public const int MOUSE_MiddleUp = 0x00000040;
        public const int MOUSE_Move = 0x00000001;
        public const int MOUSE_Absolute = 0x00008000;
        public const int MOUSE_RightDown = 0x00000008;
        public const int MOUSE_RightUp = 0x00000010;

        public static void MoveMouse(System.Drawing.Point location)
        {
            SetCursorPos(location.X, location.Y);
        }

        public static void ClickMouse(System.Drawing.Point location)
        {
            mouse_event(MOUSE_LeftDown | MOUSE_LeftUp, location.X, location.Y, 0, 0);
        }

        public static void MoveAndClick(System.Drawing.Point location)
        {
            MoveMouse(location);

            Thread.Sleep(15);

            ClickMouse(location);
        }
        #endregion

        #region WINDOW
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void ActivateWindow(string windowTitle)
        {
            IntPtr hWnd = FindWindow(null, windowTitle);
            if(hWnd != IntPtr.Zero)
                SetForegroundWindow(hWnd);
        }
        #endregion

        public const int WM_KEYDOWN = 0x100;
        public const int WM_KEYUP = 0x101;

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        //Modifiers:
        public const uint MOD_NONE = 0x0000; //[NONE]
        public const uint MOD_ALT = 0x0001; //ALT
        public const uint MOD_CONTROL = 0x0002; //CTRL
        public const uint MOD_SHIFT = 0x0004; //SHIFT
        public const uint MOD_WIN = 0x0008; //WINDOWS
                                            //CAPS LOCK:
        public const uint VK_CAPITAL = 0x14;
    }
}
