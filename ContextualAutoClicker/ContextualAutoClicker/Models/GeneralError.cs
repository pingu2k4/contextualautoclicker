﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class GeneralError : Exception
    {
        public GeneralError()
        {
        }

        public GeneralError(string message)
            : base(message)
        {
        }

        public GeneralError(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
