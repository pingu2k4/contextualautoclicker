﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class MoveRelativeAction : BaseAction
    {
        public MoveRelativeAction() : base()
        {

        }

        public MoveRelativeAction(int ID) : base(ID)
        {

        }

        public override void Run(Rectangle? location)
        {
            if (location == null)
            {
                Log("Unable to run Move Relative - location is null. Only use this action when you are not setting \"Always Trigger\" to true.", ELogLevel.ERROR);
                return;
            }
            Rectangle loc = (Rectangle)location;
            if ((loc.Width <= 0 && loc.Height <= 0))
            {
                Log("Location was null for a relative click action", ELogLevel.ERROR);
                throw new GeneralError("Location was null for a Relative Click Action.");
            }

            System.Drawing.Point p = new Point(loc.X + (loc.Width / 2), loc.Y + (loc.Height / 2));

            Log("Move mouse relative location: " + location, ELogLevel.ACTION);
            WinAPI.MoveMouse(p);
        }

        #region PROPERTIES

        #endregion
    }
}
