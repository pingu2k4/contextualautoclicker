﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    public enum ELogLevel
    {
        INFO,   // GREY
        CHECK,  // GREEN
        ACTION, // BLUE
        ERROR   // RED
    }
}
