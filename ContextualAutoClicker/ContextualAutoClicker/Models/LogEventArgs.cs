﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    public class LogEventArgs : EventArgs
    {
        public LogEventArgs() : base()
        {

        }

        public LogEventArgs(LogMessage LM) : base()
        {
            this.LM = LM;
        }

        public LogEventArgs(string message, ELogLevel level) : base()
        {
            this.LM = new LogMessage(message, level);
        }

        #region PROPERTIES
        public LogMessage LM { get; set; }
        #endregion
    }
}
