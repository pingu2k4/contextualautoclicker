﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ContextualAutoClicker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public new static App Current { get { return (App)Application.Current; } }

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnStartup(StartupEventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            Log.Info("App Startup");

// Two arguments, saved filename and number of loops.
// Number of loops is optional and is set to 0 if not specified

            if (e.Args != null && e.Args.Count() > 0)  
            {
                bool handledLoops = false;

                for (int i=0; i<e.Args.Length; i++)
                {
                    if(((String)e.Args[i]).StartsWith("-file="))
                    {
                        this.Properties["FileName"] = ((String)e.Args[i]).Substring(6);
                    }
                    else if(((String)e.Args[i]).StartsWith("-loops="))
                    {
                        this.Properties["Loops"] = ((String)e.Args[i]).Substring(7);
                        handledLoops = true;
                    }
                    else if(((String)e.Args[i]).StartsWith("-minimize"))
                    {
                        this.Properties["OpenMinimized"] = true;
                    }
                }

                if (!handledLoops)
                    this.Properties["Loops"] = -1;
            }
            base.OnStartup(e);
        }
    }
}
